//
//  VKServicesUITests.swift
//  VKServicesUITests
//
//  Created by Роман Степанов on 29.03.2024.
//

import XCTest

class VKServicesUITests: XCTestCase {
    
    func testCellTap() {
        let app = XCUIApplication()
        app.launch()
        sleep(2)
        app.tables.cells.element(boundBy: 0).tap()
    }
}
