//
//  VKServicesTests.swift
//  VKServicesTests
//
//  Created by Роман Степанов on 29.03.2024.
//

import XCTest
@testable import VKServices

class MockServicesInteractor: ServicesInteractorProtocol {
    func getServices(completion: @escaping ((VKServices) -> Void)) {
        completion(.init(body: .init(services: [
            mockServiceItem(name: "VK", serviceDescription: "Cool", link: "", iconURL: ""),
            mockServiceItem(name: "Юла", serviceDescription: "Super Cool", link: "", iconURL: "")
        ]),
                         status: 200))
    }
}

class VKServicesPresenterTests: XCTestCase {

    lazy var presenter = ServicesPresenter(interactor: self.interactor)
    var view: ServicesViewMock!
    let interactor: ServicesInteractorProtocol = MockServicesInteractor()

    override func setUp() {
        super.setUp()
        self.view = ServicesViewMock()
        self.presenter.view = self.view
    }

    func testServicesWhenViewDidLoad() {
        self.presenter.viewDidLoad()

        XCTAssertEqual(self.view.vkServices.count, 2)
    }
}
