//
//  VKServicesModuleMock.swift
//  VKServicesTests
//
//  Created by Роман Степанов on 29.03.2024.
//

import Foundation
@testable import VKServices

func mockServiceItem(name: String,
                     serviceDescription: String,
                     link: String,
                     iconURL: String) -> Service {
    Service(name: name, serviceDescription: serviceDescription, link: link, iconURL: iconURL)
}

class ServicesViewMock: ServicesViewInput {
    var vkServices: [Service] = []
    func updateModel(with vkServices: [Service]) {
        self.vkServices = vkServices
    }
}

class SeviciesOutputMock: ServicesViewOutput {
    
    func viewDidLoad() { }
    
    func loadVKServices() { }
}
