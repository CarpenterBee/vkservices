//
//  ServicesPresenter.swift
//  VKServices
//
//  Created by Роман Степанов on 28.03.2024.
//

import Foundation

protocol ServicesViewOutput {
    
    /// Загрузка view
    func viewDidLoad()
    
    /// Запрос ВК Сервисов через Interactor
    func loadVKServices()
}

protocol ServicesViewInput {
    /// - Parameters:
    ///   - vkServices: Полученный массив ВК Сервисов
    func updateModel(with vkServices: [Service])
}

final class ServicesPresenter: ServicesViewOutput {
    
    var view: ServicesViewInput?

    private let interactor: ServicesInteractorProtocol
    
    /// - Parameters:
    ///   - interactor: Интерактор для запросов в сеть
    public init(interactor: ServicesInteractorProtocol) {
        self.interactor = interactor
    }
    
    func viewDidLoad() {
        loadVKServices()
    }
    
    func loadVKServices() {
        interactor.getServices { [weak self] vkServices in
            self?.view?.updateModel(with: vkServices.body.services)
        }
    }
    
}
