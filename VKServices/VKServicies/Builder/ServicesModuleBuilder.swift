//
//  ServicesModuleBuilder.swift
//  VKServices
//
//  Created by Роман Степанов on 28.03.2024.
//

import Foundation

final class ServicesModuleBuilder {
    
    static func build() -> ServicesViewController {
        let interactor = ServicesInteractor()
        let presenter = ServicesPresenter(interactor: interactor)
        let viewController = ServicesViewController(output: presenter)
        presenter.view = viewController
        
        return viewController
    }
}
