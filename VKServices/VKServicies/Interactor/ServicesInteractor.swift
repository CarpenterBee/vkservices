//
//  ServicesInteractor.swift
//  VKServices
//
//  Created by Роман Степанов on 28.03.2024.
//

import Foundation

protocol ServicesInteractorProtocol {
    
    /// GET запрос ВК Сервисов
    /// - Parameters:
    ///   - completion:
    func getServices(completion: @escaping ((VKServices) -> Void))
}

final class ServicesInteractor: ServicesInteractorProtocol {
    
    private let networkManager: NetworkManagerProtocol
    
    /// - Parameters:
    ///   - networkManager: Сервис для запроса в сеть
    public init(networkManager: NetworkManagerProtocol = NetworkManager()) {
        self.networkManager = networkManager
    }
    
    func getServices(completion: @escaping (VKServices) -> Void) {
        guard let api = Endpoint.services().request else { return }
        
        networkManager.getRequest(with: api,
                                  model: VKServices.self) { result in
            switch result {
            case let .success(success):
                completion(success)
            case let .failure(failure):
                print(failure)
            }
        }
    }
}

