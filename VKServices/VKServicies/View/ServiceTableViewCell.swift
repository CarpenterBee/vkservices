//
//  ServiceTableViewCell.swift
//  VKServices
//
//  Created by Роман Степанов on 28.03.2024.
//

import UIKit

final class ServiceTableViewCell: UITableViewCell {
    
    // MARK: - Private Properties
    
    private struct Constants {
        static let containerHeight: CGFloat = 60.0
        static let cornerRadius: CGFloat = 5.0
        static let titleFontSize: CGFloat = 20.0
        static let subtitleFontSize: CGFloat = 12.0
        static let offset: CGFloat = 10.0
        static let imageSize: CGFloat = 60.0
        static let numberOfLines: Int = 2
    }
    
    private let iconImageView = UIImageView()
    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()
    private let containerView = UIView()
    
    private let webImageManager: WebImageProtocol = WebImageManager()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: - Public Methods
    
    public func setup(_ service: Service) {
        setupImage(service.iconURL)
        titleLabel.text = service.name
        subtitleLabel.text = service.serviceDescription
    }
    
    // MARK: - Private Methods
    
    private func setupUI() {
        backgroundColor = .black
        setupIcon()
        setupContainer()
        setupTitle()
        setupSubtitle()
    }
    
    private func setupIcon() {
        iconImageView.contentMode = .scaleAspectFill
        
        contentView.addSubview(iconImageView)
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            iconImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            iconImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.offset),
            iconImageView.widthAnchor.constraint(equalToConstant: Constants.imageSize),
            iconImageView.heightAnchor.constraint(equalToConstant: Constants.imageSize)
        ])
    }
    
    private func setupContainer() {
        contentView.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            containerView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            containerView.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: Constants.offset),
            containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.offset),
            containerView.heightAnchor.constraint(equalToConstant: Constants.containerHeight)
        ])
    }
    
    private func setupTitle() {
        titleLabel.font = UIFont.boldSystemFont(ofSize: Constants.titleFontSize)
        titleLabel.textColor = .white
        
        containerView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor)
        ])
    }
    
    private func setupSubtitle() {
        subtitleLabel.font = UIFont.boldSystemFont(ofSize: Constants.subtitleFontSize)
        subtitleLabel.numberOfLines = Constants.numberOfLines
        subtitleLabel.textColor = .white
        subtitleLabel.lineBreakMode = .byClipping
        
        containerView.addSubview(subtitleLabel)
        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            subtitleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            subtitleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            subtitleLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
    }
    
    // MARK: - Helpers
    
    private func setupImage(_ withUrl: String) {
        webImageManager.getImage(url: withUrl) { [weak self] result in
            guard let self else { return }
            switch result {
            case let .success(image):
                self.iconImageView.image = image
            case let .failure(failure):
                print(failure)
            }
        }
    }
}
