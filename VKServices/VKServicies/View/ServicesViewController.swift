//
//  ServicesViewController.swift
//  VKServices
//
//  Created by Роман Степанов on 28.03.2024.
//

import UIKit

final class ServicesViewController: UIViewController {
    
    // MARK: - Internal Properties
    
    var output: ServicesViewOutput?
    
    // MARK: - Private Properties
    
    private struct Constants {
        static let screenTitle = "Сервисы VK"
        static let cellID = String(describing: ServiceTableViewCell.self)
        static let cellHeight: CGFloat = 80.0
    }
    
    private let servicesTableView = UITableView()
    
    private var vkServices: [Service] = [] {
        didSet {
            self.servicesTableView.reloadData()
        }
    }
    
    // MARK: - Init
    
    /// - Parameters:
    ///   - output: используется для общения с presenter
    public init(output: ServicesViewOutput? = nil) {
        super.init(nibName: nil, bundle: nil)
        self.output = output
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewDidLoad()
        setupUI()
    }
    
    // MARK: - Private methods
    
    private func setupUI() {
        setupTableView()
        configureNavigationController()
        servicesTableView.register(ServiceTableViewCell.self, forCellReuseIdentifier: Constants.cellID)
    }
    
    private func setupTableView() {
        view.addSubview(servicesTableView)
        servicesTableView.backgroundColor = .black
        servicesTableView.dataSource = self
        servicesTableView.delegate = self
        servicesTableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            servicesTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            servicesTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            servicesTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            servicesTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    private func configureNavigationController() {
        navigationItem.title = Constants.screenTitle
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSMutableAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
}

// MARK: - ServicesViewController + TableViewDelegate, TableViewDataSource

extension ServicesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        vkServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellID,
                                                       for: indexPath) as! ServiceTableViewCell
        let service = vkServices[indexPath.row]
        cell.setup(service)
        cell.accessoryView = .customeRightArrow(.gray)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constants.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let websiteURL = URL(string: vkServices[indexPath.row].link) else { return }
        let application = UIApplication.shared
        
        // launch app if exist or open wevsite url
        application.open(websiteURL)
    }
}

// MARK: - ServicesViewController + ServicesViewInput

extension ServicesViewController: ServicesViewInput {
    
    func updateModel(with vkServices: [Service]) {
        self.vkServices = vkServices
    }
}
