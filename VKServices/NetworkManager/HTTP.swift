//
//  HTTP.swift
//  VKServices
//
//  Created by Роман Степанов on 28.03.2024.
//

import Foundation

public enum HTTP {
    
    enum Method: String {
        case get = "GET"
        case post = "POST"
    }
    
    enum Headers {
        
        enum Key: String {
            case contentType = "Content-Type"
        }
        
        enum Value: String {
            case applicationJson = "application/json"
        }
    }
}
