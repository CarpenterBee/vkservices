//
//  NetworkManager.swift
//  VKServices
//
//  Created by Роман Степанов on 28.03.2024.
//

import Foundation

protocol NetworkManagerProtocol {
    func getRequest<T: Decodable>(with request: URLRequest,
                                model: T.Type,
                                completion: @escaping (Result<T, Error>) -> Void)
}

final class NetworkManager: NetworkManagerProtocol {
    
    enum Errors: Error {
        case invalidURL
        case invalidState
    }
    
    private let urlSession = URLSession(configuration: .ephemeral)
    private let jsonDecoder = JSONDecoder()
    
    public func getRequest<T: Decodable>(with request: URLRequest,
                                         model: T.Type,
                                         completion: @escaping (Result<T, Error>) -> Void) {
        urlSession.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                switch(data, error) {
                case let (.some(data), nil):
                    do {
                        let reservation = try self.jsonDecoder.decode(model, from: data)
                        completion(.success(reservation))
                    } catch {
                        completion(.failure(error))
                    }
                case let (nil, .some(error)):
                    completion(.failure(error))
                default:
                    completion(.failure(Errors.invalidState))
                }
            }
        }.resume()
    }
}
