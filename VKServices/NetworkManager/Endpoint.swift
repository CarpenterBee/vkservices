//
//  Endpoint.swift
//  VKServices
//
//  Created by Роман Степанов on 28.03.2024.
//

import Foundation

public enum Endpoint {
    
    private struct Constants {
        
        //MARK: API     
        static let scheme = "https"
        static let baseURL = "publicstorage.hb.bizmrg.com"
    }
    
    case services(url: String = "/sirius/result.json")
    
    public var request: URLRequest? {
        guard let url = self.url else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = self.httpMethod
        return request
    }
    
    private var url: URL? {
        var components = URLComponents()
        components.scheme = Constants.scheme
        components.host = Constants.baseURL
        components.path = self.path
        
        return components.url
    }
    
    private var path: String {
        switch self {
        case let .services(url): url
        }
    }
    
    private var httpMethod: String {
        switch self {
        case .services: HTTP.Method.get.rawValue
        }
    }
}
