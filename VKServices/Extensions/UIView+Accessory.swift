//
//  UIView+Accessory.swift
//  VKServices
//
//  Created by Роман Степанов on 29.03.2024.
//

import UIKit

extension UIView {
    static func customeRightArrow(_ color: UIColor) -> UIImageView? {
        let image = UIImage(systemName: "chevron.right")?.withRenderingMode(.alwaysTemplate)
        let imageView = UIImageView(image: image)
        imageView.tintColor = color
        return imageView
    }
}
